import logo from './logo.svg';
import './App.css';
import FormIscrizioneComponent from './components/FormIscrizioneComponent';

function App() {
  return (
    <div className="container">
      <FormIscrizioneComponent />
    </div>
  );
}

export default App;
