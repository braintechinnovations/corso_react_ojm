import React, { Component } from 'react'

export class FormIscrizioneComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             nome: "N.D.",
             cognome: "N.D."
        }
    }
    
    invioForm = (evt) => {
        evt.preventDefault()

        console.log(evt)
       
        this.setState((state) => ({
            nome: evt.target.inputNome.value,
            cognome: evt.target.inputCognome.value
        }))
    }

    //Creare un componente che, all'inserimento dell'input crei un elenco sotto forma di tabella nella parte bassa
    //Nel form saranno presenti: 
    //Input: Nome
    //Input: Cognome
    //Textare: Note
    //Select: M/F/A

    // Nome | Cognome | Note | Sesso

    /*
    <thead>
        <tr>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
    */

    render() {
        return (
            <div>
                <h1>Sono il form</h1>

                <form onSubmit={this.invioForm}>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoNome">Nome</label>
                                <input type="text" name="inputNome" id="campoNome" placeholder="Inserisci il valore..." className="form-control"/>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoCognome">Cognome</label>
                                <input type="text" name="inputCognome" id="campoCognome" placeholder="Inserisci il valore..." className="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <button className="btn btn-primary btn-block">Inserisci</button>
                        </div>
                        <div className="col-md-4"></div>
                    </div>

                </form>

                <hr />

                <h1 className="mt-4">Dettaglio del profilo appena inserito</h1>

                <div className="row">
                    <div className="col">Nome: {this.state.nome}</div>
                </div>
                <div className="row">
                    <div className="col">Cognome: {this.state.cognome}</div>
                </div>
            </div>
        )
    }
}

export default FormIscrizioneComponent
