import React, { Component } from 'react'
import ElementoAutoComponent from './ElementoAutoComponent'

export class ElencoAutoComponent extends Component {

    constructor(props){
        super(props)

        this.automobili = ["BMW", "Lambo", "Lambo", "FIAT",  "Maserati"]
    }

    render() {

        const elencoJsx = (
            <React.Fragment>
                {this.automobili.map((obj, idx) => <ElementoAutoComponent key={idx} auto={obj} indice={idx} />)}
            </React.Fragment>
        )

        return (
            <React.Fragment>
                {elencoJsx}
            </React.Fragment>
        )
    }
}

export default ElencoAutoComponent
