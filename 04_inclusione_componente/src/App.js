import logo from './logo.svg';
import './App.css';
import React from 'react';
import ElencoAutoComponent from './components/ElencoAutoComponent';

function App() {
  return (
    <React.Fragment>
      <ElencoAutoComponent />
    </React.Fragment>
  );
}

export default App;
