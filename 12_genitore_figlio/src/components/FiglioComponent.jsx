import React, { Component } from 'react'

export class FiglioComponent extends Component {

    constructor(props) {
        super(props)

        console.log(this.props)
    
        this.state = {
             
        }
    }
    
    pulsanteCliccato = () => {
        // console.log("Hai cliccato il pulsante")
        this.props.saluta();
        this.props.secondaria("Giovanni");
    }

    render() {
        return (
            <div>
                <h1>Sono il figlio {this.props.nome}</h1>
                <button type="button" onClick={this.pulsanteCliccato}>Cliccami</button>
            </div>
        )
    }
}

export default FiglioComponent
