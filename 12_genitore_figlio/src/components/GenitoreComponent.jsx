import React, { Component } from 'react'
import FiglioComponent from './FiglioComponent'

export class GenitoreComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    salutaPersona = () => {
        console.log("Sto salutando dal genitore")
    }

    azioneSecondaria = (varNome) => {
        console.log(`Sono un'azione secondaria ${varNome}`)
    }

    render() {
        return (
            <div>
                <h1>Sono in genitore</h1>

                <FiglioComponent nome="Giovanni" saluta={this.salutaPersona} secondaria={this.azioneSecondaria}/>
            </div>
        )
    }
}

export default GenitoreComponent
