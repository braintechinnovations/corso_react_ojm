import logo from './logo.svg';
import './App.css';
import React from 'react';
import GenitoreComponent from './components/GenitoreComponent';

function App() {
  return (
    <React.Fragment>
      <GenitoreComponent />
    </React.Fragment>
  );
}

export default App;
