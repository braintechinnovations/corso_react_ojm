import React, { Component } from 'react'

export class IngredienteComponente extends Component {
    render() {
        const { nomeIng, descIng, quanIng, unimIng } = this.props

        return (
            <tr>
                <td>{nomeIng}</td>
                <td>{descIng}</td>
                <td>{quanIng}</td>
                <td>{unimIng}</td>
            </tr>
        )
    }
}

export default IngredienteComponente
