import React, { Component } from 'react'
import IngredienteComponente from '../Ingrediente/IngredienteComponente';

export class RicettaComponent extends Component {

    constructor(props) {
        super(props)
    }
    

    render() {
        const { titolo, ingred } = this.props;
        // const titolo = this.props.titolo;
        // const ingred = this.props.ingred;

        console.log(ingred)

        return (
            <div>
                <h1>{titolo}</h1>
                <table className="table table-striped">
                    <tbody>
                        {ingred.map((ingrediente, indice) => (
                            <IngredienteComponente 
                                key={indice}
                                nomeIng={ingrediente.nome}
                                descIng={ingrediente.descrizione}
                                quanIng={ingrediente.qta}
                                unimIng={ingrediente.um} />
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default RicettaComponent
