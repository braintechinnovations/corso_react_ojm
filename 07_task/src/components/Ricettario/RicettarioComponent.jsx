import React, { Component } from 'react'
import RicettaComponent from '../Ricetta/RicettaComponent'

export class RicettarioComponent extends Component {

    elencoRicette

    constructor(props) {
        super(props)
    
        this.elencoRicette = [
            {
                nome: "Torilla",
                ingredienti: [
                    {
                        nome: "Uova",
                        descrizione: "Uova biologiche",
                        qta: 2,
                        um: "PZ"
                    },
                    {
                        nome: "Patata",
                        descrizione: "Patata nostrana",
                        qta: 500,
                        um: "g"
                    }
                ]
            },
            {
                nome: "Pasta aglio e olio",
                ingredienti: [
                    {
                        nome: "Pasta",
                        descrizione: "Pasta artigianale",
                        qta: 500,
                        um: "g"
                    },
                    {
                        nome: "Olio",
                        descrizione: "Olio casereccio",
                        qta: 0.2,
                        um: "l"
                    }
                ]
            }
        ]
    }
    

    render() {
        return (
            <div>
                {this.elencoRicette.map((ricetta, indice) => 
                    <RicettaComponent 
                        key={indice}
                        titolo={ricetta.nome} 
                        ingred={ricetta.ingredienti} />
                )}
            </div>
        )
    }
}

export default RicettarioComponent
