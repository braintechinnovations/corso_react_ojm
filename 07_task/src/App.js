import logo from './logo.svg';
import './App.css';
import RicettarioComponent from './components/Ricettario/RicettarioComponent';

function App() {
  return (
    <div className="container mt-5">
      <h1>TASK</h1>
      Creare un sistema che abbia in memoria più ricette.<br />
        Ogni ricetta è composta da:

        <ul>
          <li>Nome del prodotto</li>
          <li>Descrizione del prodotto</li>
          <li>Quantità</li>
          <li>Unità di misura</li>
        </ul>
        
        {/* Es.
        Totilla:
        Uova - Uova biologiche - 2 - pz
        Patata - Patata nostrana - 500 - g

        Pasta aglio e olio
        Pasta - Pasta artigianale - 500 - g
        Olio - Olio casereccio - 0.2 - l */}

        <hr />

        <RicettarioComponent />

    </div>
  );
}

export default App;
