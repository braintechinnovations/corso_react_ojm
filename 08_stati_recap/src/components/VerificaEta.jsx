import React, { Component } from 'react'

export class VerificaEta extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             eta: this.props.eta
        }
    }
    
    compleanno = () => {
        this.setState((state) => ({eta: Number.parseInt(state.eta) + 1}))
    }

    render() {
        return (
            <div>
                <h1>La tua età è: {this.state.eta}</h1>
                <h3>Sei: {this.state.eta >= 18 ? "Maggiorenne" : "Minorenne"}</h3>
                <button type="button" onClick={this.compleanno}>Aggiorna eta</button>
            </div>
        )
    }
}

export default VerificaEta

/*
    - Creare un piccolo sistema che:
    tenga conto dei risultati di partite di squadre di calcio/tennis/...

    Abbia su due colonne e sulla stessa riga le squadre coinvolte nel match
    Ai lati delle squadre voglio due pulsanti in grado di incrementare e decremenetare il valore
*/