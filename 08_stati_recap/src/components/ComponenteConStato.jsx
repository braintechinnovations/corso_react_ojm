import React, { Component } from 'react'

export class ComponenteConStato extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             numero: Number.parseInt(this.props.numero)
        }

        console.log("sono nel Constructor")
    }

    pulsanteCliccato = () => {
        setInterval(() => {
            this.setState((state, props) => ({ numero: state.numero + 1 }))
        }, 1000);
    }

    render() {
        console.log("sono nel Render")
        
        return (
            <div>
                <h1>Il numero selezionato è: {this.state.numero}</h1>  
                <button type="button" onClick={this.pulsanteCliccato}>Aggiorna automaticamente</button>
            </div>
        )
    }

    componentDidMount(){
        console.log("sono nel DidMount")
    }

    componentDidUpdate(){
        console.log("**** sono nel DidUpdate ****")
    }
}

export default ComponenteConStato
