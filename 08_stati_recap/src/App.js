import logo from './logo.svg';
import './App.css';
import ComponenteConStato from './components/ComponenteConStato';
import React from 'react';
import VerificaEta from './components/VerificaEta';

function App() {
  return (
    <React.Fragment>
      {/* <ComponenteConStato numero="88"/> */}

      <VerificaEta eta="10"/>

    </React.Fragment>
  );
}

export default App;
