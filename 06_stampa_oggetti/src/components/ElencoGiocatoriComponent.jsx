import React, { Component } from 'react'
import GiocatoreComponent from './GiocatoreComponent'

export class ElencoGiocatoriComponent extends Component {

    constructor(props){
        super(props)

        this.elenco = [
            {
                nome: "Thiago",
                cognome: "Motta",
                numero: 11
            },
            {
                nome: "Cristiano",
                cognome: "Malgioglio",
                numero: 12
            },
            {
                nome: "Er",
                cognome: "Capitano",
                numero: 10
            }
        ]
    }

    render() {
        return (
            <table className="table table-striped">
                <tbody>

                    {this.elenco.map((pers) => <GiocatoreComponent num={pers.numero} nom={pers.nome} cog={pers.cognome}/>)}

                </tbody>
            </table>
        )
    }
}

export default ElencoGiocatoriComponent
