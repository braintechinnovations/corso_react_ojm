import React, { Component } from 'react'

export class GiocatoreComponent extends Component {
    render() {
        const { num, nom, cog } = this.props

        return (
            <tr>
                <td>{num}</td>
                <td>{nom}</td>
                <td>{cog}</td>
            </tr>
        )
    }
}

export default GiocatoreComponent
