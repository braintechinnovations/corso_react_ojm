import logo from './logo.svg';
import './App.css';
import React from 'react'
import ElencoGiocatoriComponent from './components/ElencoGiocatoriComponent';

function App() {
  return (
    <div className="container">
      <h1>Task 2</h1>
      <p>
      Prova a stampare una tabella con qualche giocatore della squadra dell'italia <br />
      Numero | Nome | Cognome
      </p>

      <hr />

      <ElencoGiocatoriComponent />
    </div>
  );
}

export default App;
