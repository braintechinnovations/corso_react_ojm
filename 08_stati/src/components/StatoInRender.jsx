import React, { Component } from 'react'

export class StatoInRender extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             eta: "0"
        }

        this.aggiornaEta()
    }

    aggiornaEta(){
        setInterval(() => {
            this.setState((state, props) => ({  eta: Number.parseInt(this.eta) + 1 } ))
        }, 1000);
    }
    
    render() {
        

        return (
            <div>
                {this.state.eta}<br />
                {this.state.eta >= 18 ? "Maggiorenne" : "Minorenne"}
            </div>
        )
    }
}

export default StatoInRender
