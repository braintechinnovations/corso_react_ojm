import React, { Component } from 'react'

export class CicloDiVita extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             eta: 5
        }

        console.log("Sono il costruttore")
    }

    componentDidMount(){
        console.log("Sono il metodo componentDidMount")

        // Metodo rapido per il set di un valore di stato
        this.setState((state, props) => ({eta: 55}))

        // Metodo per il set di un valore di stato con elaborazione
        // this.setState((state, props) => {
        //     let valore = 85;

        //     return {
        //         eta: state.eta + valore
        //     }
        // } )
    }

    render() {
        console.log("Sono il metodo render()")
        return (
            <div>
                <h1>{this.state.eta}</h1>
            </div>
        )
    }
}

export default CicloDiVita
