import logo from './logo.svg';
import './App.css';
import React from 'react';
import StatoSemplice from './components/StatoSemplice'
import StatoInRender from './components/StatoInRender';
import CicloDiVita from './components/CicloDiVita';

function App() {
  return (
    <React.Fragment>
      {/* <StatoSemplice /> */}
      {/* <StatoInRender /> */}
      <CicloDiVita />
    </React.Fragment>
  );
}

export default App;
