import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class RigaComponent extends Component {

    constructor(props) {
        super(props)
    
        const {contatto} = this.props
        const { id, nome, cognome, telefono} = contatto;

        this.state = {
             id,
             nome,
             cognome,
             telefono
        }
    }
    

    render() {
        const urlPersona = "/dettaglio/" + this.state.id

        return (
            <tr>
                {/* <td>{this.state.id}</td> */}
                <td>{this.state.nome}</td>
                <td>{this.state.cognome}</td>
                <td>
                    <Link to={urlPersona}>{this.state.telefono}</Link>
                </td>
            </tr>
        )
    }
}

export default RigaComponent
