import React, { Component } from 'react'
import RigaComponent from './RigaComponent'
import axios from 'axios'

export class ElencoComponent extends Component {

    urlRiferimento = "http://localhost:4000/persone/list"

    constructor(props) {
        super(props)
    
        this.state = {
             elencoPersone: [],
             showErrore: false
        }
    }

    aggiornaElenco = () => {
        axios.get(this.urlRiferimento)
        .then((risultato) => {
            this.setState((stato) => ({elencoPersone: risultato.data}))
        })
        .catch((errore) => {
            this.setState((stato) => ({showErrore: true}))
        })
    }
    
    componentDidMount(){
        console.log("Sono nel componentDidMount")
        this.aggiornaElenco();

        this.aggiornamentoAutomatico = setInterval(() => {
            this.aggiornaElenco();
        }, 2000);
    }

    componentWillUnmount(){
        console.log("Sono nel componentWillUnmount")
        clearInterval(this.aggiornamentoAutomatico)
    }

    clickPulsanteAggiorna = () => {
        this.aggiornaElenco();
    }

    render() {
        let htmlErrore                                              //Variabile NULL
        if(this.state.showErrore){
            htmlErrore = (
                <div className="alert alert-danger" role="alert" >
                    Errore di connessione al server
                </div>
            )
        }

        return (
            <div>
                <div className="row">
                    <div className="col-md-10">
                        <h1>Elenco contatti</h1>
                    </div>
                    <div className="col-md-2">
                        <button type="button" className="btn btn-outline-primary btn-block" onClick={this.clickPulsanteAggiorna}>Aggiorna</button>
                    </div>
                </div>

                {htmlErrore && htmlErrore}

                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.elencoPersone.map((persona, indice) => (
                            <RigaComponent key={indice} contatto={persona} />
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ElencoComponent
