import axios from 'axios'
import React, { Component } from 'react'

export class DettaglioPersonaComponent extends Component {

    urlDettaglio = "http://127.0.0.1:4000/persone/"

    constructor(props) {
        super(props)
    
        const {identificatore} = this.props

        this.state = {
            identificatore,
            persona: {
                id: null,
                nome: null,
                cognome: null,
                telefono: null
            },
            showModifica: false
        }
    }
    
    componentDidMount(){
        axios.get(this.urlDettaglio + this.state.identificatore).then(risultato => {
            console.log(risultato)

            let arrayRis = risultato.data
            if(arrayRis.length > 0){
                this.setState((stato) => ({
                    persona: arrayRis[0]
                }))
            }
            else{
                alert("Errore, non trovato!")
            }

            console.log(this.state)
        })
    }

    eliminaPersona = () => {
        axios.delete(this.urlDettaglio + this.state.identificatore)
        .then(risultato => {  
            document.location.href = "/"
        })
        .catch(errore => {
            console.log(errore)
            alert("Attenzione, errore di eliminazione")
        })
    }

    abilitaModifica = () => {
        this.setState((stato) => ({
            showModifica: !stato.showModifica
        }))
    }

    modificaPersona = (evt) => {
        evt.preventDefault();

        let personaTemp = {
            nome: evt.target.inputNome.value,
            cognome: evt.target.inputCognome.value,
            telefono: evt.target.inputTelefono.value,
        }

        axios.put(this.urlDettaglio + this.state.persona.id, personaTemp).then(
            risultato => {
                document.location.reload()
            }
        )
        .catch(errore => {
            console.log(errore)
            alert("Errore, modifica non effettuata")
        })
    }

    render() {

        const codiceDettaglio = (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">
                        {this.state.persona.nome && this.state.persona.nome}&nbsp;
                        {this.state.persona.cognome && this.state.persona.cognome}
                    </h5>
                    <p className="card-text">Tel: {this.state.persona.telefono && this.state.persona.telefono}</p>
                    <button type="button" className="btn btn-outline-danger" onClick={this.eliminaPersona}>Elimina</button>
                    <button type="button" className="btn btn-outline-danger" onClick={this.abilitaModifica}>Modifica</button>
                </div>
            </div>
        )

        const codiceModifica = (
            <form onSubmit={this.modificaPersona}>
                <div className="form-group">
                    <label htmlFor="campoNome">Nome</label>
                    <input type="text" className="form-control" name="inputNome" id="campoNome" defaultValue={this.state.persona.nome} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoCognome">Cognome</label>
                    <input type="text" className="form-control" name="inputCognome" id="campoCognome" defaultValue={this.state.persona.cognome} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoTelefono">Telefono</label>
                    <input type="number" className="form-control" name="inputTelefono" id="campoTelefono" defaultValue={this.state.persona.telefono} />
                </div>

                <button className="btn btn-outline-success btn-block">Effettua la modifica</button>
                <button type="button" className="btn btn-outline-info btn-block mt-2" onClick={this.abilitaModifica}>Annulla</button>
            </form>
        )

        return (
            
            <React.Fragment>
                {this.state.showModifica ? codiceModifica : codiceDettaglio}
                
            </React.Fragment>
        )
    }
}

export default DettaglioPersonaComponent
