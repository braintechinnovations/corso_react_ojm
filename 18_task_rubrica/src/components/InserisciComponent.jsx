import axios from 'axios'
import React, { Component } from 'react'

export class InserisciComponent extends Component {

    urlInserimento = "http://127.0.0.1:4000/persone/insert"

    constructor(props) {
        super(props)
    
        this.state = {
             showTuttoOk: false
        }
    }

    inserisciPersona = (evt) => {
        evt.preventDefault()

        let personaTemp = {
            nome: evt.target.inputNome.value,
            cognome: evt.target.inputCognome.value,
            telefono: evt.target.inputTelefono.value,
        }

        axios.post(this.urlInserimento, personaTemp).then((risultato) => {

            if(risultato.data.status && risultato.data.status === 'success'){
                this.setState((stato) => ({
                    showTuttoOk: true
                }))

                setTimeout(() => {
                    document.location.href = "/"
                }, 2000);
            }
        })

        console.log(personaTemp)
    }

    render() {

        const tuttoOk = (
            <div class="alert alert-success" role="alert">
                Persona inserita con successo!
            </div>
        )

        const pulsanteInserisci = (
            <React.Fragment>
                <button className="btn btn-outline-success btn-block">Inserisci</button>
            </React.Fragment>
        )

        return (
            <div>
                <h1>SONO INSERISCI</h1>

                <form onSubmit={this.inserisciPersona}>
                    <div className="form-group">
                        <label htmlFor="campoNome">Nome</label>
                        <input type="text" className="form-control" name="inputNome" id="campoNome" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="campoCognome">Cognome</label>
                        <input type="text" className="form-control" name="inputCognome" id="campoCognome" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="campoTelefono">Telefono</label>
                        <input type="number" className="form-control" name="inputTelefono" id="campoTelefono" />
                    </div>

                    {this.state.showTuttoOk ? tuttoOk : pulsanteInserisci}

                </form>
            </div>
        )
    }
}

export default InserisciComponent

/**
 *  Creare un sistema completo di:
 *  DB - BE - FE
 * 
 *  Creare un sistema di gestione locali,
 *  ogni locale è caratterizzato da:
 *  - Nome
 *  - Descrizione
 *  - Città
 *  - Provincia
 *  - Regione
 *  - Latitudine
 *  - Longitudine
 *  - Voto              <----------------------------- da 1 a 5
 * 
 *  LATO ADMIN, tutte le path iniziano per /admin/...
 *  1. CRUD locali
 *  1a. L'amministratore vede in diretta le votazioni!
 *  2. CHALLENGE - RESET VOTO LOCALE (fatelo alla fine!)
 * 
 *  LATO PUBBLICO
 *  1. Elenco dei locali visualizzabile sotto forma di tabella o lista (come volete, sorprendetemi!)
 *  2. Possibilità di votare un locale, possibilmente una volta sola (utilizzate le tecnologie che conoscete)
 *  3. CHALLENGE - Possibilità di filtrare tra i locali per Città/Provincia/Regione
 */

/**
 * STEP: 
 * 1. Sviluppo il DB
 * 2. Metto qualche dato nel DB
 * 3. Sviluppo BE
 * 4. Test del BE con Postman
 * 5. Sviluppo del FE
 */