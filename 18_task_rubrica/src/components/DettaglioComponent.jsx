import React, { Component } from 'react'
import { useParams } from 'react-router-dom'
import DettaglioPersonaComponent from './DettaglioPersonaComponent';

export function DettaglioComponent() {

    let {id} = useParams();

    return (
        <React.Fragment>
            <DettaglioPersonaComponent identificatore={id} />
        </React.Fragment>
    )
}

export default DettaglioComponent
