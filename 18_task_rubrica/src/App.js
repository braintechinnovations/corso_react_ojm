import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom'
import ElencoComponent from './components/ElencoComponent';
import InserisciComponent from './components/InserisciComponent';
import DettaglioComponent from './components/DettaglioComponent';

function App() {
  return (

    <Router>

      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/">Rubrica</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">

          <ul className="navbar-nav">
            <li className="nav-item">
              <Link className="nav-link" to="/">Elenco</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/inserisci">Inserisci</Link>
            </li>
          </ul>

        </div>
      </nav>

      <div className="container mt-5">
        <Routes>
          <Route path="/" element={<ElencoComponent />}></Route>
          <Route path="/inserisci" element={<InserisciComponent />}></Route>
          <Route path="/dettaglio/:id" element={<DettaglioComponent />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
