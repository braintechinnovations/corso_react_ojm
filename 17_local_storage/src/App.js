import logo from './logo.svg';
import './App.css';
import React from 'react';
import LocalStorageComponent from './components/LocalStorageComponent';

function App() {
  return (
    <div className="container">
      <LocalStorageComponent />
    </div>
  );
}

export default App;
