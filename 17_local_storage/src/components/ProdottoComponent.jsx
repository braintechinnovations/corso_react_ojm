import React, { Component } from 'react'

export class ProdottoComponent extends Component {

    constructor(props) {
        super(props)
    
        const {prodotto} = this.props;

        this.state = {
            prodotto
        }
    }
    

    render() {
        return (
            <tr>
                <td>{this.state.prodotto.nome}</td>
            </tr>
        )
    }
}

export default ProdottoComponent
