import React, { Component } from 'react'
import ProdottoComponent from './ProdottoComponent'

export class LocalStorageComponent extends Component {

    constructor(props) {
        super(props)

        // let prodottiAttuali = JSON.parse(localStorage.getItem("prodotti"))

        // if(!prodottiAttuali){
        //     prodottiAttuali = []
        // }

        let prodottiAttuali = localStorage.getItem("prodotti") ? JSON.parse(localStorage.getItem("prodotti")) : []
    
        this.state = {
             elencoProdotti: prodottiAttuali
        }
    }
    
    aggiungiProdotto = (evt) => {
        evt.preventDefault()

        let elencoAttuale = this.state.elencoProdotti;
        elencoAttuale.push({
            nome: evt.target.inputNome.value
        })

        this.setState((stato) => ({elencoProdotti : elencoAttuale}))

        localStorage.setItem("prodotti", JSON.stringify(this.state.elencoProdotti))
    }

    render() {
        return (
            <React.Fragment>

                <div className="row">
                    <div className="col">
                        <form onSubmit={this.aggiungiProdotto}>
                            <div className="form-group">
                                <label htmlFor="campoNome">Prodotto</label>
                                <input 
                                    type="text" 
                                    name="inputNome" 
                                    id="campoNome" 
                                    className="form-control" 
                                    placeholder="Inserisci il valore"
                                ></input>
                            </div>

                            <button className="btn btn-block btn-outline-success">Inserisci</button>
                        </form>
                    </div>
                </div>

                <hr />

                <table className="table table-striped">
                    <thead>
                        <tr>
                            <td>Nome Prodotto</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.elencoProdotti.map((prod, indice) => (
                            <ProdottoComponent key={indice} prodotto={prod} />
                        ))}
                    </tbody>
                </table>

            </React.Fragment>
        )
    }
}

export default LocalStorageComponent

/**
 * Sviluppare una SPA che mantenga informazioni relative a:
 * Nome | Cognome | Telefono
 */