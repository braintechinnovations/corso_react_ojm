import logo from './logo.svg';
import './App.css';
import PassaggioParametri from './components/PassaggioParametri'
import { PassaggioParametriClassi } from './components/PassaggioParametriClassi';
import RigaComponentClasse from './components/RigaComponentClasse';
import RigacomponentFunzione from './components/RigaComponentFunzione';
import RigaComponentDefaultType from './components/RigaComponentDefaultType';


function App() {
  return (
    <div>
      <PassaggioParametri nome="Mario" cognome="Rossi" />
      <PassaggioParametri nome="Giovanni" cognome="Pace" />
      <PassaggioParametri nome="Valeria" />
      <PassaggioParametriClassi nome="Marika" cognome="Verdi" />

      <hr />

      <table>
        <tbody>
          <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Telefono</th>
          </tr>

          <tr>
            <RigaComponentClasse nome="Giovanni" cognome="Pace" telefono="123456"/>
          </tr>
          <tr>
            <RigaComponentClasse nome="Mario" cognome="Rossi" telefono="654321"/>
          </tr>
          <tr>
            <RigaComponentClasse nome="Valeria" cognome="" telefono=""/>
          </tr>
          <tr>
            <RigaComponentDefaultType nome="Mirko" cognome="Marchi" telefono="87965"/>
          </tr>
          <tr>
            <RigaComponentDefaultType nome="Marika"/>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
