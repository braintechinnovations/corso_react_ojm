import React, { Component } from 'react'
import propTypes from 'prop-types'

export class RigaComponentDefaultType extends Component {
    render() {
        console.log(this.props)

        return (
            <React.Fragment>
                <td>{this.props.nome}</td>
                <td>{this.props.cognome}</td>
                <td>{this.props.telefono}</td>
                <td>
                    <button>Cliccami</button>
                </td>
            </React.Fragment>
        )
    }
}

RigaComponentDefaultType.propTypes = {
    nome: propTypes.string,
    cognome: propTypes.string,
    telefono: propTypes.number
}

export default RigaComponentDefaultType
