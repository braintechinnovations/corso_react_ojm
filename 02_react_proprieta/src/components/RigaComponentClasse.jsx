import React, { Component } from 'react'


export class RigaComponentClasse extends Component {

    utente

    constructor(props){
        super(props)

        this.utente = new Object();
        this.utente.nome = this.props.nome ? this.props.nome : "N.D."; 
        this.utente.cognome = this.props.cognome ? this.props.cognome : "N.D.";
        this.utente.telefono = this.props.telefono ? this.props.telefono : "N.D.";
    }

    render() {
        return (
            <React.Fragment>
                <td>{this.utente.nome}</td>
                <td>{this.utente.cognome}</td>
                <td>{this.utente.telefono}</td>
                <td>
                    <button>Cliccami</button>
                </td>
            </React.Fragment>
        )
    }
}

export default RigaComponentClasse
