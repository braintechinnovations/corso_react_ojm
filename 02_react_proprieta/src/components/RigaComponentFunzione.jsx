import React from "react";
import propType from 'prop-types'

function RigacomponentFunzione(props){

    console.log(props)

    return(
        <React.Fragment>
                <td>{props.nome}</td>
                <td>{props.cognome}</td>
                <td>{props.telefono}</td>
                <td>
                    <button>Cliccami</button>
                </td>
            </React.Fragment>
    )
}

RigacomponentFunzione.defaultProps = {
    nome: "N.D.",
    cognome: "N.D.",
    telefono: "N.D."
}

RigacomponentFunzione.propType = {
    nome: propType.string,
    telefono: propType.number
}

export default RigacomponentFunzione