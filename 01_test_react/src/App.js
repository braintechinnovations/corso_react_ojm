import './App.css';
import FiglioFunzione from './components/FiglioFunzione'
import FiglioClasse from './components/FiglioClasse'
import FiglioArrow from './components/FiglioArrow';

function App() {
  return (
    <div>
      <h1>Ciao Spiderman</h1>
      <p>Speriamo non sia un fiasco!</p>

      <FiglioFunzione />  {/* Questo è il Componente Funzionale classico */}
      <FiglioClasse />    {/* Questo è il Componente Funzionale con dichiarazione di classe */}
      <FiglioArrow />     {/* Questo è il Componente Funzionale con Arrow Function */}

      {
        //Ciao sono un commento javascript
        console.log("Ciao Giovanni, sono un blocco commento")
      }
    </div>
  );
}   

export default App;
