class Persona {
    nome: String

    constructor(){

    }

    setNome(varNome: String) : void{
        if(varNome.trim().length == 0){
            this.nome = "N.D."
        }
        else{
            this.nome = varNome;
        }
    }

    getNome() : String {
        return this.nome.toUpperCase();
    }
}

let gio = new Persona()
gio.setNome("Giovanni")

console.log(this.getNome())