import React, { Component } from 'react'

export class HomeComponent extends Component {

    //FASE DI MOUNTING
    constructor(props) {
        super(props)
    
        this.state = {
             
        }

        console.log("----> Costruito")
    }
    

    componentDidMount(){
        console.log("----> componentDidMount")
    }

    //FASE DI UPDATING
    componentDidUpdate(){
        console.log("----> componentDidUpdate")
    }

    //FASE DI UNMOUNT
    componentWillUnmount(){
        console.log("----> componentWillUnmount")
    }
    

    render() {
        return (
            <div>
                <h1>Sono la pagina principale</h1>
            </div>
        )
    }
}

export default HomeComponent
