import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import HomeComponent from './components/HomeComponent';
import ContactComponent from './components/ContactComponent';
import AboutComponent from './components/AboutComponent';

//Necessita il plugin: https://www.npmjs.com/package/react-router-dom

function App() {
  return (
    <Router>
      
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">Navbar</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/" className="nav-link">Home</Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link">Chi siamo</Link>
            </li>
            <li className="nav-item">
              <Link to="/contact" className="nav-link">Contatti</Link>
            </li>
          </ul>
        </div>
      </nav>

      <div className="container">
        <Routes>
          <Route path="/" element={<HomeComponent />}></Route>
          <Route path="/about" element={<AboutComponent />}></Route>
          <Route path="/contact" element={<ContactComponent />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
