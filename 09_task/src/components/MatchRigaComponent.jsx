import React, { Component } from 'react'
import SquadraComponent from './SquadraComponent'

export class MatchRigaComponent extends Component {

    constructor(props) {
        super(props)

        const {sqCasa, sqOspi} = this.props

        this.state = {
             casa: sqCasa,
             ospi: sqOspi
        }
    }
    
    render() {
        return (
            <tr>
                <td>
                    <SquadraComponent nome={this.state.casa} />
                </td>
                <td>
                    <SquadraComponent nome={this.state.ospi} />
                </td>
            </tr>
        )
    }
}

export default MatchRigaComponent
