import React, { Component } from 'react'

export class SquadraComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
             nomeSquadra: this.props.nome,
             goalCounter: 0,
             errore: false
        }
    }
    
    incrementaGoal = () => {
        this.setState((state) => ({ goalCounter: state.goalCounter + 1, errore: false }))
    }
    
    decrementaGoal = () => {
        if(this.state.goalCounter > 0)
            this.setState((state) => ({ goalCounter: state.goalCounter - 1 }))
        else
            this.setState((state) => ({ errore: true }))
    }

    render() {
        const bloccoErrore = (
            <div className="alert alert-primary" role="alert">
                Errore, valore negativo!
            </div>
        )

        return (
            <React.Fragment> 
                <div className="row">
                    <div className="col">
                        <button className="btn btn-info" onClick={this.decrementaGoal}>-</button>
                    </div>
                    <div className="col">
                        <h5>{this.state.nomeSquadra}<br />{this.state.goalCounter}</h5>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.incrementaGoal}>+</button>
                    </div>
                </div>

                {this.state.errore && bloccoErrore}                
            </React.Fragment>
        )
    }
}

export default SquadraComponent
