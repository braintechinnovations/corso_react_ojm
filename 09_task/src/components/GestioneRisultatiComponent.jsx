import React, { Component } from 'react'
import MatchRigaComponent from './MatchRigaComponent'

export class GestioneRisultatiComponent extends Component {

    constructor(props) {
        super(props)

        this.elencoMatch = [
            {
                casa: "Inter",
                ospite: "Roma"
            },
            {
                casa: "Juventus",
                ospite: "Milan"
            },
            {
                casa: "Fiorentina",
                ospite: "Cagliari"
            },
        ]
    
        this.state = {
             match: this.elencoMatch
        }
    }
    

    render() {
        return (
            <div className="container mt-5 text-center">
                <h1>Elenco dei match</h1>
                
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Squadra 1</th>
                            <th>Squadra 2</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.match.map((elemento, indice) => (
                        <MatchRigaComponent 
                            key={indice}
                            sqCasa={elemento.casa} 
                            sqOspi={elemento.ospite} 
                        />
                    ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default GestioneRisultatiComponent
