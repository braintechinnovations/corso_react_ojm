import logo from './logo.svg';
import './App.css';
import React from 'react'
import GestioneRisultatiComponent from './components/GestioneRisultatiComponent';

function App() {
  return (
    <React.Fragment>
      <GestioneRisultatiComponent />
    </React.Fragment>
  );
}

export default App;
