import React, { Component } from 'react'

export class FormController extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }

    invioForm = (evt) => {
        evt.preventDefault()                            //Evito il comportamento di default del form che cambia pagina!
        // console.log(evt)
        // console.log(evt.target.inputNome.value)
        // console.log(evt.target.inputCognome.value)

        let persona = {
            nome: evt.target.inputNome.value,
            cognome: evt.target.inputCognome.value
        }

        console.log(persona)
    }

    alCambiamento = () => {
        console.log("è stata effettuata una modifica!")
    }

    render() {
        console.log("Sto invocando il Render()")

        return (
            <>
                <h1>Form di inserimento dati</h1>
                <form onSubmit={this.invioForm}>
                    <div className="form-group">
                        <label htmlFor="campoNome">Nome</label>
                        <input 
                            type="text" 
                            name="inputNome" 
                            id="campoNome" 
                            className="form-control"
                            onChange={this.alCambiamento}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="campoCognome">Cognome</label>
                        <input 
                            type="text" 
                            name="inputCognome" 
                            id="campoCognome" 
                            className="form-control"
                            onChange={this.alCambiamento}
                        />
                    </div>

                    <button type="submit" className="btn btn-success btn-block">Registrami</button>
                </form>
            </>
        )
    }

    componentDidUpdate(){
        console.log("Sto invocando il DidUpdate")
    }
}

export default FormController
