import logo from './logo.svg';
import './App.css';
import FormController from './components/FormController';

function App() {
  return (
    <div className="container">
      <FormController />
    </div>
  );
}

export default App;
