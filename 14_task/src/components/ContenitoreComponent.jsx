import React, { Component } from 'react'
import InserimentoComponent from './InserimentoComponent'
import StampaComponent from './StampaComponent'

export class ContenitoreComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             elencoLibri: []
        }
    }
    
    aggiungiAdElenco = (libro) => {
        let elencoNuovo = this.state.elencoLibri;
        elencoNuovo.push(libro)

        this.setState((stato) => ({elencoLibri: elencoNuovo}))
        console.log(this.state)
    }

    render() {
        return (
            <div className="text-center">
                <h1>Gestione libri</h1>

                <div className="row">
                    <div className="col-md-6">
                        <InserimentoComponent aggiunta={this.aggiungiAdElenco}/>
                    </div>
                    
                    <div className="col-md-6">
                        <StampaComponent elenco={this.state.elencoLibri} />
                    </div>
                </div>
            </div>
        )
    }
}

export default ContenitoreComponent
