import React, { Component } from 'react'
import RigaLibroComponent from './RigaLibroComponent'

export class StampaComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             elencoLibri: this.props.elenco
        }
    }

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>Titolo</th>
                        <th>ISBN</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.elencoLibri.map((libro, indice) => (
                        <RigaLibroComponent key={indice} libro={libro} />
                    ))}
                </tbody>
            </table>
        )
    }
}

export default StampaComponent
