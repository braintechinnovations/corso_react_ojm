import React, { Component } from 'react'

export class RigaLibroComponent extends Component {

    constructor(props) {
        super(props)
    
        const {libro} = this.props          //this.libro = this.props.libro
        const {titolo, isbn} = libro        //this.titolo = this.libro.titolo
                                            //this.isbn = this.libro.isbn

        this.state = {
             titolo,                        //titolo: titolo
             isbn                           //isbn: isbn
        }
    }
    

    render() {
        const urlRiferimento = `http://www.google.it?q=${this.state.isbn}`

        return (
            <tr>
                <td>{this.state.titolo}</td>
                <td>
                    <strong>
                        <a href={urlRiferimento}>{this.state.isbn}</a>
                    </strong>
                </td>
            </tr>
        )
    }
}

export default RigaLibroComponent
