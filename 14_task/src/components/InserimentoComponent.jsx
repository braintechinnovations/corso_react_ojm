import React, { Component } from 'react'

export class InserimentoComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             titolo: "",
             isbn: ""
        }
    }
    
    inserimentoLibro = () => {
        console.log("Inserimento libro")
        this.props.aggiunta(this.state)                  //Richiamo l'aggiunta dell'elemento all'interno del padre

        // this.setState((stato) => ({
        //     titolo: "",
        //     isbn: ""
        // }))
    }

    eventoChange = (evt) => {
        let statoTemp = {};
        switch(evt.target.name){
            case "inputTitolo":
                statoTemp.titolo = evt.target.value;
                break;
            case "inputIsbn":
                statoTemp.isbn = evt.target.value;
                break;
            default:
                alert("Errore!")
                return;
        }

        this.setState((stato) => (statoTemp))    
    }

    render() {
        return (
            <div className="text-left mt-5">
                <h4>Nuovo libro</h4>

                <div className="form-group">
                    <label htmlFor="campoTitolo">Titolo</label>
                    <input type="text" onChange={this.eventoChange} name="inputTitolo" id="campoTitolo" className="form-control" placeholder="Inserisci valore" />
                </div>
                
                <div className="form-group">
                    <label htmlFor="campoIsbn">ISBN</label>
                    <input type="text" onChange={this.eventoChange} name="inputIsbn" id="campoIsbn" className="form-control" placeholder="Inserisci valore" />
                </div>

                <button type="button" className="btn btn-outline-success btn-block" onClick={this.inserimentoLibro}>Inserisci libro</button>

            </div>
        )
    }
}

export default InserimentoComponent
