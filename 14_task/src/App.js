import logo from './logo.svg';
import './App.css';
import React from 'react'
import ContenitoreComponent from './components/ContenitoreComponent';

function App() {
  return (
    <div className="container mt-3">
      <ContenitoreComponent />
    </div>
  );
}

export default App;
