import React from 'react'

import { Form, Button } from 'react-bootstrap'

function FormComponent(props){


    return(
        <React.Fragment>

            <Form.Group className="mt-3">
                <Form.Label>Nome</Form.Label>
                <Form.Control placeholder="INserisci il valore" name="inputNome" id="campoNome"></Form.Control>
            </Form.Group>


            <Form.Group className="mt-3 mb-3">
                <Form.Label>Cognome</Form.Label>
                <Form.Control placeholder="INserisci il valore" name="inputCognome" id="campoCognome"></Form.Control>
            </Form.Group>


            <Button variant="success">Inserisci il valore</Button>
            

        </React.Fragment>
    )
}

export default FormComponent

/**
 * Creare una SPA che permetta l'inserimento di un solo componente di:
 * Dettaglio del proprietario
 * -- Nome, Cognome, CF, Indirizzo (campo composito)
 * Dettaglio dei veicoli posseduti
 * -- Per ogni veicolo: Targa, Data di acquisto, Modello, Marca
 * 
 * Trovare una soluzione per salvare i dati tramite Hooks all'interno del LocalStorage (se volte con REST API)
 * E che utilizzi react-boostrap come lib grafica. Dove potete non utilizzate i className ma le componenti di rb
 */