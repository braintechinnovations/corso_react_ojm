import logo from './logo.svg';
import storm from './immagini/storm_2.png'
import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css'

// import Container from 'react-bootstrap/Container'
import { Col, Container, Row } from 'react-bootstrap'
import FormComponent from './components/FormComponent';

function App() {

  return (
    <Container>
      <Row>
        <Col md={6}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Col>
        <Col md={6}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Col>
      </Row>
      
      <Row className="mt-5">
        <Col>
          <FormComponent />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
