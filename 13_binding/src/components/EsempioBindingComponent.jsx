import React, { Component } from 'react'

export class EsempioBindingComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             nome: "N.D.",
             cognome: "N.D."
        }
    }
    
    eventoModifica = (evt) => {
        console.log(evt.target.name)
        let objTemp = {

        }
        if(evt.target.name == "nome"){
            objTemp.nome = evt.target.value
        }
        if(evt.target.name == "cognome"){
            objTemp.cognome = evt.target.value
        }

        if(evt.target.value.length >= 3){
            this.setState((stato) => (objTemp))
        }
    }

    inserisciStudente = () => {
        console.log(this.state)
    }

    render() {
        return (
            <div>

                <div className="form-group">
                    <label htmlFor="campoNome">Nome</label>
                    <input 
                        type="text" 
                        name="nome" 
                        id="campoNome" 
                        className="form-control"
                        onChange={this.eventoModifica}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="campoCognome">Cognome</label>
                    <input 
                        type="text" 
                        name="cognome" 
                        id="campoCognome" 
                        className="form-control"
                        onChange={this.eventoModifica}
                    />
                </div>

                <button type="button" className="btn btn-info" onClick={this.inserisciStudente}>Inserisci Studente</button>

                <br />

                <strong>Nome: {this.state.nome}</strong><br />
                <strong>Cognome: {this.state.cognome}</strong>
            </div>
        )
    }
}

export default EsempioBindingComponent

//Creare un componente genitore che contiene due altri sottocomponenti (figli)
//Il primo componente si occupa di inserire i valori relativi ad un libro (Titolo e ISBN)
//Il secondo componente si occupa soltanto della stampa dell'elenco dei libri