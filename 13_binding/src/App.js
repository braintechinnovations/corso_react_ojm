import logo from './logo.svg';
import './App.css';
import React from 'react';
import EsempioBindingComponent from './components/EsempioBindingComponent';

function App() {
  return (
    <div className="container mt-2">
      <EsempioBindingComponent />
    </div>
  );
}

export default App;
