import logo from './logo.svg';
import './App.css';
import React from 'react';
import ElencoPersoneController from './components/ElencoPersoneController';

function App() {
  return (
    <React.Fragment>
      <h1>TASK 1</h1>
      <p>PROVA A STAMPARE L'ELENCO DEI NOMI NELLA NOSTRA ROOM DI MEET</p>
      
      <hr />

      <ElencoPersoneController />
    </React.Fragment>
  );
}

export default App;
