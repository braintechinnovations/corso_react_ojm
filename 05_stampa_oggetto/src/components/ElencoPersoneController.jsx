import React, { Component } from 'react'

export class ElencoPersoneController extends Component {

    constructor(props){
        super(props)

        this.elencoPartecipanti = [
            "Christian", "Gennaro", "Mauro", "Federico", "Fernanda"
        ]
    }

    render() {
        return (
            <ul>
                {this.elencoPartecipanti.map((varPersona, varIndice) => <li key={varIndice}>{varPersona}</li>)}
            </ul>
        )
    }
}

export default ElencoPersoneController
