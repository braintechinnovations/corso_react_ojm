import logo from './logo.svg';
import './App.css';
import React from 'react';
import GestioneStudenteComponent from './components/GestioneStudenteComponent';

function App() {
  return (
    <div className="container mt-5">
      <GestioneStudenteComponent />
    </div>
  );
}

export default App;
