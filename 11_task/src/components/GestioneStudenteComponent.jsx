import React, { Component } from 'react'
import RigaStudenteComponent from './RigaStudenteComponent'

export class GestioneStudenteComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             elencoStudenti: [],
             contatore: 0
        }
    }
    
    invioForm = (evt) =>  {
        evt.preventDefault()

        let studente = {
            nome: evt.target.campoNome.value,
            cognome: evt.target.campoCognome.value,
            sesso: evt.target.campoSesso.value,
            note: evt.target.campoNote.value,
        }

        let arrayTemporaneo = this.state.elencoStudenti;
        arrayTemporaneo.push(studente)

        this.setState((state) => ({
            elencoStudenti: arrayTemporaneo
        }))
    }

    incrementaElenco = () => {
        this.setState((stato) => ({contatore: stato.contatore + 1}))
    }

    render() {
        return (
            <React.Fragment>
                <h1>Sono il form</h1>

                <form onSubmit={this.invioForm}>

                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="campoNome">Nome</label>
                                <input type="text" name="inputNome" id="campoNome" placeholder="Inserisci il valore..." className="form-control"/>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="campoCognome">Cognome</label>
                                <input type="text" name="inputCognome" id="campoCognome" placeholder="Inserisci il valore..." className="form-control"/>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="campoSesso">Sesso</label>
                                <select name="inputSesso" id="campoSesso" className="form-control">
                                    <option value="A">Altro</option>
                                    <option value="F">Femmina</option>
                                    <option value="M">Maschio</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label htmlFor="campoNote">Note</label>
                                <textarea name="inputNote" id="campoNote" className="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <button className="btn btn-primary btn-block">Inserisci</button>
                        </div>
                        <div className="col-md-4"></div>
                    </div>
                </form>

                <hr />

                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <h4>Numero di studenti inseriti: {this.state.contatore}</h4>
                    </div>
                    <div className="col-md-4"></div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Sesso</th>
                            <th>Note</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.elencoStudenti.map((studente, indice) => (
                            <RigaStudenteComponent key={indice} stud={studente} incremento={this.incrementaElenco}/>
                        ))}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default GestioneStudenteComponent
