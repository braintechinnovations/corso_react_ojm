import React, { Component } from 'react'

export class RigaStudenteComponent extends Component {

    constructor(props) {
        super(props)

        const { stud } = this.props;
        const { cognome, nome, note, sesso } = stud;
    
        this.state = {
            nome,
            cognome,
            note,
            sesso
        }

        // this.props.incremento()
        
    }
    

    render() {

        return (
            <tr>
                <td>{this.state.nome}</td>
                <td>{this.state.cognome}</td>
                <td>
                    {this.state.sesso == "A" && "Altro"}
                    {this.state.sesso == "F" && "Femmina"}
                    {this.state.sesso == "M" && "Maschio"}
                </td>
                <td>{this.state.note}</td>
            </tr>
        )
    }
}

export default RigaStudenteComponent
