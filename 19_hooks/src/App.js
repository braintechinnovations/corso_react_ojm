import logo from './logo.svg';
import './App.css';
import HookSempliceComponent from './components/HookSempliceComponent';
import HookComplessoComponent from './components/HookComplessoComponent';

function App() {
  return (
    <div className="App">
      {/* <HookSempliceComponent/> */}

      <HookComplessoComponent />
    </div>
  );
}

export default App;
