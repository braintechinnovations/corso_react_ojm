import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';

function HookComplessoComponent(props) {

    const [articolo, setArticolo] = useState(
        {
            titolo: "Titolo della notizia",
            testo: "Testo della notizia un pochino più lungo del normale!"
        }
    )

    const [persona, setPersona] = useState({
        nome: "Giovanni",
        cognome: "Pace"
    })

    useEffect(() => {
        console.log("useEffect invocato per Articolo")
    }, [articolo])

    useEffect(() => {
        console.log("useEffect invocato per Persona")
    }, [persona])

    useEffect(() => {
        console.log("Vengo invocato sempre")
    }, [])

    const aggiornaTitolo = () => {
        // setArticolo({titolo: "Nuovo titolo"})       // ;( sto eliminando gli altri attributi
        
        setArticolo((prev) => (
            {            
                ...prev,
                titolo: "Nuovo titolo",
                // utente: "Giovanni"
            }
        ))
    }

    const aggiornaPersona = () => {
        setPersona((prev) => (
            {
                ...prev,
                nome: "Mario"
            }
        ))
    }

    return (
        <React.Fragment>
            <h1>{articolo.titolo}</h1>
            <p>{articolo.testo}</p>
            <p><i>{persona.nome} {persona.cognome}</i></p>
            <button type="button" onClick={aggiornaTitolo}>Aggiorna titolo</button>
            <button type="button" onClick={aggiornaPersona}>Aggiorna persona</button>
        </React.Fragment>
    )

}

export default HookComplessoComponent;