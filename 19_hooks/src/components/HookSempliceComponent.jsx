import React, { useState } from "react";


function HookSempliceComponent(props) {

    const [contatore, setContatore] = useState(props.contatore)
    const [nome, setNome] = useState(props.nome)


    const aggiornaContatore = () => {
        setContatore(
            (ciccio) => (Number.parseInt(ciccio) + 1)
        )
    }

    return (
        <div>
            <h1>Ecco il valore attuale del contatore:</h1>
            <h1>{contatore}</h1>

            <button type="button" onClick={aggiornaContatore}>Incrementa</button>
        </div>
    )

}

export default HookSempliceComponent;