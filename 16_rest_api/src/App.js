import logo from './logo.svg';
import './App.css';
import React from 'react';
import ListComponent from './components/ListComponent';

function App() {
  return (
    <React.Fragment>
      <ListComponent />
    </React.Fragment>
  );
}

export default App;
