import React, { Component } from 'react'

export class PersonaComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             nome: this.props.name,
             email: this.props.email
        }
    }
    

    render() {
        return (
            <li>
                {this.state.nome} - {this.state.email}
            </li>
        )
    }
}

export default PersonaComponent
