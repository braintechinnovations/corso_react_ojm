import React, { Component } from 'react'
import axios from 'axios'
import PersonaComponent from './PersonaComponent'

export class ListComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            elencoDipendenti: []
        }
    }
    
    componentDidMount(){
        axios.get('https://gorest.co.in/public/v1/users')
            .then((risultato) => {
                console.log(risultato.data.data)

                this.setState((stato) => ({
                    elencoDipendenti: risultato.data.data
                }))
            }).catch(() => {
                console.log("SOno disconnesso :(")
            })
    }

    render() {
        return (
            <div>
                <h1>Lista degli oggetti:</h1>
                <ul>
                    {this.state.elencoDipendenti.map((dipendente, indice) => (
                        <PersonaComponent 
                            key={indice}
                            name={dipendente.name}
                            email={dipendente.email} />
                            ))}
                </ul>
            </div>
        )
    }
}

export default ListComponent
