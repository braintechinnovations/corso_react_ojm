import logo from './logo.svg';
import './App.css';
import PersonaComponent from './components/PersonaComponent';
import StudenteComponent from './components/StudenteComponent';
import StampaArrayComponent from './components/StampaArrayComponent';
import StampaArraySeparatoComponent from './components/StampaArraySeparatoComponent';

function App() {
  return (
    <div>
      {/* 
      <PersonaComponent nome="Giovanni" cognome="Pace" />
      <PersonaComponent nome="Mario" />

      <StudenteComponent nome="Valeria" cognome="Verdi" matricola="AB123456" /> 
      <StudenteComponent nome="Maria" cognome="Viola" /> 
      */}

      {/* <StampaArrayComponent /> */}
      <StampaArraySeparatoComponent />
    </div>
  );
}

export default App;
