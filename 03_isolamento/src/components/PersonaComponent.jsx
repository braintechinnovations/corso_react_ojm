import React, { Component } from 'react'

export class PersonaComponent extends Component {

    render() {                                          //Isolamento del render
        const { nome, cognome, telefono } = this.props; 

        return (
            <div>
                <h1>Nome: {nome}</h1>
                <h2>Cognome: {cognome}</h2>
                <h2>Telefono: {telefono}</h2>
                
                <hr />
            </div>
        )
    }
    
}

export default PersonaComponent
