import React, { Component } from 'react'

export class StudenteComponent extends Component {

    constructor(props){
        super(props)

        this.nome = this.props.nome ? this.props.nome.toUpperCase() : "N.D.";                //this.nome è permesso perché in App.js ho fatto la New StudenteCOmponent()
        this.cognome = this.props.cognome ? this.props.cognome.toUpperCase() : "N.D.";
        this.matricola = this.props.matricola ? this.props.matricola.toUpperCase() : "N.D.";
    }

    render() {
        return (
            <div>
                <h1>Nome: {this.nome}</h1>
                <h2>Cognome: {this.cognome}</h2>
                <h2>matricola: {this.matricola}</h2>
                
                <hr />
            </div>
        )
    }
}

export default StudenteComponent
