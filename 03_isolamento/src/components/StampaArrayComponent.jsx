import React, { Component } from 'react'

export class StampaArrayComponent extends Component {

    constructor(props){
        super(props)

        this.automobili = ["BMW", "Lambo", "FIAT",  "Maserati"]
    }

    render() {
        return (
            <div>
                <h1>Elenco automobili</h1>
                <ul>
                    {this.automobili.map(automobile => <li>{automobile}</li>)}
                </ul>
            </div>
        )
    }
}

export default StampaArrayComponent
