import React, { Component } from 'react'

export class StampaArraySeparatoComponent extends Component {

    constructor(props){
        super(props)

        this.automobili = ["BMW", "Lambo", "FIAT",  "Maserati"]
    }

    render() {

        const elencoReact = (
            <ul>
                {this.automobili.map(auto => <li>{auto}</li>)}
            </ul>
        )

        return (
            <div>
                <h1>Elenco Automobili</h1>
                <hr />
                    {elencoReact}
                <hr />
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis aspernatur eaque voluptates impedit? Labore iure provident voluptas repellendus neque earum nihil incidunt dignissimos nemo accusantium atque beatae delectus, animi aliquid!</p>
            </div>
        )
    }
}

export default StampaArraySeparatoComponent
